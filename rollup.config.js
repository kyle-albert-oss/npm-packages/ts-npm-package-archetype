/* eslint-disable @typescript-eslint/no-var-requires */
import autoExternal from "rollup-plugin-auto-external";
import nodeResolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import typescript from "@rollup/plugin-typescript";
import multiInput from "rollup-plugin-multi-input";

import pkg from "./package.json";

const { SINGLE_FILE_BUNDLE, PACKAGE_TYPE, PACKAGE_TYPE_BROWSER, FILE_EXTENSIONS } = require("./build-config");

const extensions = FILE_EXTENSIONS;
const entryPoint = SINGLE_FILE_BUNDLE ? "src/index.ts" : ["src/**/*.ts", "!src/**/*.test.ts", "!src/**/__tests__/**"];
const sourcemap = "inline";

const nodeResolveOpts = {
	extensions,
};
if (PACKAGE_TYPE === PACKAGE_TYPE_BROWSER) {
	nodeResolveOpts.builtins = false;
}

// order matters
const sharedPlugins = [
	SINGLE_FILE_BUNDLE ? null : multiInput(),
	autoExternal(),
	nodeResolve(nodeResolveOpts),
	typescript({
		tsconfig: "./tsconfig.dist.json",
	}),
	commonjs({ extensions }),
].filter((p) => p != null);

const bundles = [
	SINGLE_FILE_BUNDLE
		? {
				input: entryPoint,
				output: [
					{ file: pkg.main, format: "cjs", sourcemap },
					{ file: pkg.module, format: "es", sourcemap },
				],
				plugins: [...sharedPlugins],
		  }
		: {
				input: entryPoint,
				output: [
					{ dir: "dist/cjs", format: "cjs", sourcemap },
					{ dir: "dist/esm", format: "es", sourcemap },
				],
				plugins: [...sharedPlugins],
		  },
];

if (PACKAGE_TYPE === PACKAGE_TYPE_BROWSER && pkg.browser) {
	bundles.push({
		input: entryPoint,
		output: {
			name: "index",
			file: pkg.browser,
			format: "umd",
		},
		plugins: [...sharedPlugins],
	});
}

export default bundles;
