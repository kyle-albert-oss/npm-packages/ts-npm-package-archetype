import * as childProcesses from "child_process";
import * as path from "path";

import chalk from "chalk";
// eslint-disable-next-line @typescript-eslint/camelcase
import * as del from "del";
import log from "fancy-log";
import * as fs from "fs-extra";
import * as gulp from "gulp";
import inquirer from "inquirer";
import * as undertaker from "undertaker";
import { Dictionary } from "ts-essentials";

export type GulpTask = undertaker.Task;

const { task } = gulp;

export const successIcon = "✅";
export const warningIcon = "⚠️";
export const errorIcon = "❌";
export const deleteIcon = "💥";
export const runIcon = "👟";

export const logSuccess = (str: string) => log(successIcon, str);
export const logError = (err: string | Error) => log.error(errorIcon, err);
export const logWarn = (str: string) => log.warn(warningIcon, str);
export const logInfo = (str: string) => log.info(str);
export const logDelete = (str: string) => log.warn(deleteIcon, str);
export const logRun = (str: string) => log(runIcon, str);

export const inlineTask = (name: string, fn: gulp.TaskFunction) => {
	task(name, fn);
	return task(name);
};

export const inlineSyncTask = (name: string, fn: () => void) =>
	inlineTask(name, (cb) => {
		try {
			fn();
			cb();
		} catch (e) {
			cb(e);
		}
	});

export const mkdirpTask = (name: string, dirPath: string | string[]) =>
	inlineSyncTask(name, () => {
		const paths: string[] = typeof dirPath === "string" ? [dirPath] : dirPath;
		for (const pth of paths) {
			const resolvedPath = path.resolve(process.cwd(), pth);
			fs.mkdirpSync(resolvedPath);
			logSuccess(chalk`{green Directory {bold ${resolvedPath}} green exists.}`);
		}
	});

type DelSync = typeof del.sync;
export const rmTask = (name: string, fn: (d: DelSync) => ReturnType<DelSync>) =>
	inlineSyncTask(name, () => {
		// eslint-disable-next-line @typescript-eslint/unbound-method
		const result = fn(del.sync);
		if (result) {
			result.forEach((deletedPath) => logDelete(chalk`{yellow Deleted {bold ${deletedPath}}}`));
		}
	});

export const execCmd = async (cmd: string, opts?: childProcesses.SpawnOptions) =>
	new Promise((r, rj) => {
		logRun(chalk`{blue Running: {bold.inverse ${cmd}}}`);
		const cmdProgram = cmd.trim().split(/\s+/)[0];
		const npmBinPath = path.resolve(process.cwd(), "node_modules/.bin");
		if (fs.existsSync(path.resolve(npmBinPath, cmdProgram))) {
			cmd = path.resolve(npmBinPath, cmd);
		}
		// seems to support dynamic stdio (loading bars, etc)
		childProcesses
			.spawn(cmd, { cwd: process.cwd(), stdio: "inherit", shell: true, ...opts, env: { ...process.env, ...opts?.env } })
			.on("error", (err) => {
				logError(err);
				rj(err);
			})
			.on("exit", (code) => (code != null && code > 0 ? rj(code) : r()));
	});

export const sudoRmTask = (name: string, filePath: string, confirm: boolean = true) => {
	inlineTask(name, async () => {
		const dir = path.resolve(process.cwd(), filePath);
		if (!fs.existsSync(dir)) {
			return;
		}
		if (confirm) {
			const answer = await inquirer.prompt([
				{ type: "confirm", name: "continue", message: chalk`{yellow Really {bold.inverse sudo rm -rf ${dir}}? (No)}`, default: false },
			]);
			if (!answer.continue) {
				throw new Error(chalk`{red Aborting task ${name}. Exiting...}`);
			}
		} else {
			logRun(`{yellow Executing {bold.inverse sudo rm -rf ${dir}}}`);
		}
		await execCmd(`sudo rm -rf ${dir}`);
	});
};

export const execTaskFn = (cmd: string, opts?: childProcesses.SpawnOptions): gulp.TaskFunction => async () => execCmd(cmd, opts);

export const execTask = (name: string, cmd: string, opts?: childProcesses.SpawnOptions) => inlineTask(name, execTaskFn(cmd, opts));

type ParamValue = string | number | boolean | null | undefined;
interface ICmdOpts {
	conditionalParams?: Dictionary<ParamValue>;
	env?: Dictionary<ParamValue>;
	params?: readonly string[];
}
export const cmd = (cmdStr: string, opts: ICmdOpts = {}): string => {
	const { conditionalParams, env, params } = opts;

	let envStr = env
		? Object.entries(env)
				.filter(([k, v]) => v != null)
				.map(([k, v]) => `${k}=${v}`)
				.join(" ")
		: null;
	if (envStr) {
		envStr = `cross-env ${envStr}`;
	}

	const paramArr = [
		...(params ?? []),
		...(conditionalParams
			? Object.entries(conditionalParams)
					.filter(([k, v]) => !!v)
					.map(([k]) => k)
			: []),
	];

	return [envStr, cmdStr, paramArr.join(" ")].filter((str) => !!str).join(" ");
};
