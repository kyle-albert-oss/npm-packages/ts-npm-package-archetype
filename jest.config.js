/* eslint-disable @typescript-eslint/no-var-requires */

const { PACKAGE_TYPE, PACKAGE_TYPE_NODE } = require("./build-config");

/**
 * https://jestjs.io/docs/en/configuration
 */
module.exports = {
	globals: {
		"ts-jest": {
			tsConfig: "tsconfig.test.json",
		},
	},
	preset: "ts-jest",
	testEnvironment: PACKAGE_TYPE === PACKAGE_TYPE_NODE ? "node" : "jsdom", // "jsdom" for browser, "node" for node only package
	moduleFileExtensions: ["js", "jsx", "ts", "tsx"],
	modulePathIgnorePatterns: ["<rootDir>/coverage/", "<rootDir>/dist/", "<rootDir>/out/"],
	//	setupFilesAfterEnv: ["./tests/setup-tests.ts"],
	transform: {
		"\\.(tsx?)$": "ts-jest",
	},
};
