const PACKAGE_TYPE_BROWSER = "browser";
const PACKAGE_TYPE_NODE = "node";

const PACKAGE_TYPE = PACKAGE_TYPE_NODE;

/**
 * Single file dist package.json fields
 *
 * "main": "dist/index.cjs.js",
 * "module": "dist/index.esm.js",
 * "browser": "dist/index.umd.js",
 * "types": "dist/index.d.ts",
 *
 * Multi-file dist package json fields
 *
 * "main": "dist/cjs/index.js",
 * "module": "dist/esm/index.js",
 * "browser": "dist/umd/index.umd.js",
 * "types": "dist/types/index.d.ts",
 *
 */
const SINGLE_FILE_BUNDLE = true;

const FILE_EXTENSIONS = [".js", ".ts", ".tsx"];

module.exports = {
	PACKAGE_TYPE,
	PACKAGE_TYPE_BROWSER,
	PACKAGE_TYPE_NODE,
	SINGLE_FILE_BUNDLE,
	FILE_EXTENSIONS,
};
