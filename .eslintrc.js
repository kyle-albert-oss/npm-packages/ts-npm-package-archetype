const { PACKAGE_TYPE, PACKAGE_TYPE_BROWSER, PACKAGE_TYPE_NODE } = require("./build-config");

module.exports = {
	plugins: ["@typescript-eslint"],
	parser: "@typescript-eslint/parser",
	parserOptions: {
		project: "./tsconfig.eslint.json",
	},
	extends: [
		"eslint:recommended",
		"plugin:@typescript-eslint/eslint-recommended",
		"plugin:@typescript-eslint/recommended",
		"plugin:@typescript-eslint/recommended-requiring-type-checking",
		"prettier",
		"prettier/@typescript-eslint",
	],
	env: {
		browser: PACKAGE_TYPE === PACKAGE_TYPE_BROWSER,
		commonjs: PACKAGE_TYPE === PACKAGE_TYPE_NODE,
		node: PACKAGE_TYPE === PACKAGE_TYPE_NODE,
	},
	rules: {
		"prefer-const": [
			"warn",
			{
				destructuring: "all",
			},
		],
		"require-await": "off",
		"@typescript-eslint/ban-ts-ignore": "warn",
		"@typescript-eslint/explicit-function-return-type": "off",
		"@typescript-eslint/interface-name-prefix": [
			"warn",
			{
				prefixWithI: "always",
				allowUnderscorePrefix: false,
			},
		],
		"@typescript-eslint/no-inferrable-types": "off",
		"@typescript-eslint/require-await": "warn",
	},
};
