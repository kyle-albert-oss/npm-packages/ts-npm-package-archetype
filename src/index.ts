const foo = (input?: string): number | string => input ?? 1_000_000;
export default foo;
