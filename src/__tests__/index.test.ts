import { AssertTrue, IsExact } from "conditional-type-checks";
import foo from "../index";

describe("Foo", () => {
	it("bars", () => {
		expect(foo()).toEqual(1_000_000);
		expect(foo("something")).toEqual("something");

		type T1 = AssertTrue<IsExact<ReturnType<typeof foo>, string | number>>;
	});
});
