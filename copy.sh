# usage: copy.sh project-name
if [[ "$1" == "" ]]; then
	echo "No argument for the name! Exiting..."
	exit 1
fi
rsync -ax --exclude 'node_modules/' --exclude '.git/' --exclude 'dist/' --exclude 'coverage/' --exclude '*.iml' --exclude 'copy.sh' . ../${1}
cd ../${1}
git init
git remote add origin git@gitlab.com:kyle-albert-oss/npm-packages/${1}.git
git add ./.gitignore
git commit -m "gitignore"
npm i
git add .
