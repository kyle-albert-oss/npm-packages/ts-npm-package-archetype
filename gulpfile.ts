import * as gulp from "gulp";
import { execTask, GulpTask, rmTask, cmd } from "./gulp-util";
import { SINGLE_FILE_BUNDLE } from "./build-config";

const { dest, parallel, series, src, task } = gulp;

const srcDir = "src";
const jestCmd = (opts?: { ci?: boolean; coverage?: boolean }) => cmd("jest", { conditionalParams: { "--coverage": opts?.coverage } });
const eslintCmd = (opts?: { fix?: boolean }): string => cmd(`eslint '**/*.{js,jsx,ts,tsx}'`, { conditionalParams: { "--fix": opts?.fix } });

//
// CLEAN
//

rmTask("clean", (d) => d(["./coverage", "./dist", "./out"]));

//
// LINT
//

execTask("lint", eslintCmd());
execTask("lint:fix", eslintCmd({ fix: true }));

//
// TEST
//

execTask("test:cyclic", `madge -c --warning --ts-config ./tsconfig.json --extensions js,jsx,ts,tsx --no-spinner ./${srcDir}`);
execTask("test:lint", eslintCmd());
execTask("test:unit", jestCmd());
execTask("test:unit-with-coverage", jestCmd({ coverage: true }));
const baseTestTasks: GulpTask[] = ["test:cyclic", "test:lint"];

const getTestTasks = (opts?: { ci?: true; coverage?: boolean }): GulpTask[] => {
	const tasks = [...baseTestTasks];
	tasks.push(opts && opts.coverage ? "test:unit-with-coverage" : "test:unit");
	return tasks;
};

//
// BUILD
//

const buildTasks: GulpTask[] = [
	execTask("build:rollup", "rollup -c"),
	execTask(
		"build:tsc:d",
		cmd("tsc", {
			conditionalParams: {
				"--isolatedModules false": SINGLE_FILE_BUNDLE,
				"--outFile ./dist/index.d.ts": SINGLE_FILE_BUNDLE,
				"--outDir ./dist/types": !SINGLE_FILE_BUNDLE,
			},
			params: ["-p ./tsconfig.dist.json", "--emitDeclarationOnly"],
		})
	),
];

//
// DIST
//

const distTasks: GulpTask[] = [];

//
// TOP-LEVEL TASKS
//

const getTopLevelBuildTasks = (opts?: { ci?: boolean; coverage?: boolean }): GulpTask[] => {
	const tasks: GulpTask[] = ["clean"];
	if (!opts?.ci) {
		tasks.push(opts?.coverage ? "build:test-with-coverage" : "build:test");
	}
	tasks.push("build:compile", ...distTasks);
	return tasks;
};

task("test", parallel(...getTestTasks()));
task("build:test", parallel(...getTestTasks()));
task("build:test-with-coverage", parallel(...getTestTasks({ coverage: true })));

task("build:compile", parallel(...buildTasks));
task("build", series(...getTopLevelBuildTasks()));

//
// CI TOP-LEVEL TASKS, we want to be as granular as possible to split into pipeline stages
//

task("ci:test", parallel(...getTestTasks({ ci: true, coverage: true })));
task("ci:build", series(...getTopLevelBuildTasks({ ci: true })));
